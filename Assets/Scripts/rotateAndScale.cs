﻿using UnityEngine;
using System.Collections;

public class rotateAndScale : MonoBehaviour
{
    public float rotSpeed = 20;
    public float scaleSpeed = 50;
    float posX;
    float rotY;
    float scaleMove;

    void Update()
    {
        if (Input.touchCount == 1)
        {
            // GET TOUCH 0
            Touch touch0 = Input.GetTouch(0);

            if (touch0.phase == TouchPhase.Began)
            {
                posX = touch0.deltaPosition.x;
            }

            // APPLY ROTATION
            if (touch0.phase == TouchPhase.Moved)
            {
                rotY = touch0.deltaPosition.x * rotSpeed * Mathf.Deg2Rad;
                scaleMove = touch0.deltaPosition.y * scaleSpeed * Mathf.Deg2Rad;
                transform.RotateAround(Vector3.up, -rotY);

                transform.localScale += new Vector3(scaleMove, scaleMove, scaleMove);

                if (touch0.deltaPosition.x < posX)
                {

                }
            }
        }
        }

    void OnMouseDrag()
    {
        float rotX = Input.GetAxis("Mouse X") * rotSpeed * Mathf.Deg2Rad;
        float rotY = Input.GetAxis("Mouse Y") * rotSpeed * Mathf.Deg2Rad;

        transform.RotateAround(Vector3.up, -rotX);
        transform.RotateAround(Vector3.right, rotY);
    }
}
